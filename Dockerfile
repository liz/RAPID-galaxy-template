FROM centos:centos7
MAINTAINER Liz Wendland

RUN yum install -y git
RUN yum install -y java-1.7.0-openjdk
RUN git clone https://github.com/galaxyproject/galaxy/ && cd galaxy && git checkout -b master origin/master

ENTRYPOINT '/galaxy/run.sh'
# forexample: CMD [ '-help']

EXPOSE 8080
